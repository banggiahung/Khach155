﻿using Khach155.Data;
using Khach155.Models;
using Khach155.Models.BangMainViewModel;
using Khach155.Models.LuuTruMuaViewModel;
using Khach155.Models.RutTienViewModel;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SEMPJ2.Models.HoSoGiaoDichViewModels;

namespace Khach155.Areas.Admin.Controllers
{
    [Area("Admin")]

    public class AdminFbController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly IConfiguration _configuration;
        public AdminFbController(ApplicationDbContext context, IConfiguration configuration)
        {
            _context = context;
            _configuration = configuration;
        }
        public IActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> Index(BangMainCRUDViewModels model)
        {
            if (ModelState.IsValid)
            {
                model.UserId = 1;
                model.Cancel = false;
                BangMain _bang = new BangMain();
                _bang.Id = model.Id;
                _bang.NguoiBan = model.NguoiBan;
                _bang.GiaCa = model.GiaCa;
                _bang.Cancel = model.Cancel;
                _bang.UserId = model.UserId;
                _context.Add(_bang);
                await _context.SaveChangesAsync();

            }
            return Ok(model);

        }
        [HttpGet]
        public IActionResult GetDataMua()
        {
			var a = from obj in _context.LuuTruMua
					join _user in _context.DataUser on obj.UserId equals _user.Id
					where obj.MuonBan == false
					select new LuuTruMuaCRUDViewModels
					{
						Id = obj.Id,
						UserId = _user.Id,
						GiaMua = obj.GiaMua,
						MuonBan = obj.MuonBan,
						MuaCuaAi = obj.MuaCuaAi,
						TenUser = _user.UserName,
                        SoLuongMua = obj.SoLuongMua,
                        SoTienThanhToan = obj.SoTienThanhToan
					};
			return Ok(a.ToList());
		}
        [HttpGet]
        public IActionResult GetDataBan()
        {
            var a = from obj in _context.LuuTruBan
                    join _user in _context.DataUser on obj.UserId equals _user.Id
                    select new LuuTruMuaCRUDViewModels
                    {
                        Id = obj.Id,
                        UserId = _user.Id,
                        GiaMua = obj.GiaBan,
                        MuaCuaAi = obj.BanChoAi,
                        TenUser = _user.UserName,
                        SoLuongMua = obj.SoLuongBan,
                        SoTienThanhToan = obj.SoTienThanhToanBan
                    };
            return Ok(a.ToList());
        }
        // cập nhật số tiền khi có lệnh nạp
        [HttpPost]
        public async Task<IActionResult> CapNhatLenh([FromForm] HoSoGiaoDichCRUDViewModels _lenh)
        {
            try
            {
                var data = await _context.HoSoGiaoDich.FindAsync(_lenh.ID);
                data.TrangThaiGiaoDich = _lenh.TrangThaiGiaoDich;
                _context.Update(data);

                if (_lenh.TrangThaiGiaoDich == true)
                {
                    var dataUser = await _context.DataUser.FirstOrDefaultAsync(u => u.Id == _lenh.UserIDInfo);
                    if (dataUser != null)
                    {
                        dataUser.TienDangCo = dataUser.TienDangCo + _lenh.SoTienNap;

                        _context.Update(dataUser);
                    }
                    await _context.SaveChangesAsync();
                    return Ok(_lenh);
                }
                else
                {
                    return NotFound();
                }

            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        // cập nhật số tiền khi có lệnh rut

        [HttpPost]
        public async Task<IActionResult> CapNhatLenhRut([FromForm] RutTien _lenh)
        {
            try
            {
                var data = await _context.RutTien.FindAsync(_lenh.Id);
                data.TrangThaiGiaoDich = _lenh.TrangThaiGiaoDich;
                _context.Update(data);

                if (_lenh.TrangThaiGiaoDich == true)
                {
                    //var dataUser = await _context.DataUser.FirstOrDefaultAsync(u => u.Id == _lenh.UserId); 
                    //if (dataUser != null)
                    //{
                    //    dataUser.TienDangCo = dataUser.TienDangCo - _lenh.SoTienRut;
                    //    _context.Update(dataUser); 
                    //}
                    await _context.SaveChangesAsync();
                    return Ok(_lenh);
                }
                else
                {
                    return NotFound();
                }

            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }
       
        [HttpGet]
        // lấy tất cả giao dịch nạp tiền
        // api 
        public async Task<IActionResult> HoSoXuLyData()
        {
            var _GetGridItem = GetGridItemNap();

            return Ok(_GetGridItem);

        }
        // lấy tất cả giao dịch nạp tiền
        private IQueryable<HoSoGiaoDichCRUDViewModels> GetGridItemNap()
        {
            try
            {
                return(from hoSo in _context.HoSoGiaoDich
                        join _user in _context.DataUser on hoSo.UserIDInfo equals _user.Id

                        select new HoSoGiaoDichCRUDViewModels
                        {
                            ID = hoSo.ID,
                            UserIDInfo = hoSo.UserIDInfo,
                            ThoiGianGiaoDich = hoSo.ThoiGianGiaoDich,
                            TrangThaiGiaoDich = hoSo.TrangThaiGiaoDich,
                            MoTa = hoSo.MoTa,
                            SoTienNap = hoSo.SoTienNap,
                            TenUserDisplay = _user.UserName,
                            CodeLenh = hoSo.CodeLenh


                        }).OrderByDescending(x => x.ID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [HttpGet]
        public async Task<IActionResult> HoSoXuLyRut()
        {
            var _GetGridItemRut = GetGridItemRut();

            return Ok(_GetGridItemRut);

        }
        // lấy tất cả giao dịch rút tiền

        private IQueryable<RutTienCRUDViewModels> GetGridItemRut()
        {
            try
            {
                return (from hoSo in _context.RutTien
                        join _user in _context.DataUser on hoSo.UserId equals _user.Id
                        select new RutTienCRUDViewModels
                        {
                            Id = hoSo.Id,
                            TenBankUser = hoSo.TenBankUser,
                            STKBankUser = hoSo.STKBankUser,
                            SoTienRut = hoSo.SoTienRut,
                            UserId = hoSo.UserId,
                            ThoiGianRut = hoSo.ThoiGianRut,
                            TrangThaiGiaoDich = hoSo.TrangThaiGiaoDich,
                            TenUser = _user.UserName

                        }).OrderByDescending(x => x.Id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
